﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace Estadisticos
{
    public class Estadistico
    {
        public string NombreDeColumna { get; set; }
        public string TipoDeDato { get; set; }
        public int Conteo { get; set; }
        public double Maximo { get; set; }
        public double Minimo { get; set; }
        public double Media { get; set; }
        public double Mediana { get; set; }
        public double DesviacionEstandard { get; set; }
        public double Varianza { get; set; }
        public double Caurtil1 { get; set; }
        public double Caurtil2 { get; set; }
        public double Caurtil3 { get; set; }
        public string TipoDeVariable { get; set; }
    }
}
