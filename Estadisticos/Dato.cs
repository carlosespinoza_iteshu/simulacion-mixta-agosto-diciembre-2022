﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Estadisticos
{
    public class Dato
    {
        public int Store { get; set; }
        public string Date { get; set; }
        public float Weekly_Sales { get; set; }
        public bool Holiday_Flag { get; set; }
        public float Temperature { get; set; }
        public float Fuel_Price { get; set; }
        public float CPI { get; set; }
        public float Unemployment { get; set; }
    }
}
