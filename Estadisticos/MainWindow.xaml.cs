﻿using CsvHelper;
using Microsoft.Win32;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Estadisticos
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Dato> datosOrigen;
        List<Estadistico> estadisticos;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnSeleccionaArchivo_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Archivos CSV|*.csv";
            dialog.Title = "Abrir archivo CSV";
            if(dialog.ShowDialog().Value)
            {
                string archivo=dialog.FileName;
                txbArchivo.Text = archivo;
            }
            else
            {
                txbArchivo.Text = "";
            }
        }

        private void btnProcesar_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txbArchivo.Text))
            {
                using (var reader = new StreamReader(txbArchivo.Text))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    datosOrigen = csv.GetRecords<Dato>().ToList();
                }
                estadisticos = new List<Estadistico>();
                estadisticos.Add(CrearEstadistico(datosOrigen.Select(d => d.Store).ToList(), "Store", "Int", "Discreta"));
                estadisticos.Add(CrearEstadistico(datosOrigen.Select(d => d.Weekly_Sales).ToList(), "Weekly_Sales", "Double", "Continua"));
                estadisticos.Add(CrearEstadistico(datosOrigen.Select(d => d.Temperature).ToList(), "Temperature", "Double", "Continua"));
                estadisticos.Add(CrearEstadistico(datosOrigen.Select(d => d.Fuel_Price).ToList(), "Fuel_Price", "Double", "Continua"));
                estadisticos.Add(CrearEstadistico(datosOrigen.Select(d => d.CPI).ToList(), "CPI", "Double", "Continua"));
                estadisticos.Add(CrearEstadistico(datosOrigen.Select(d => d.Unemployment).ToList(), "Unemployment", "Double", "Continua"));
                dtgDatos.ItemsSource = estadisticos;

                //tabla ya creada
                OxyPlot.PlotModel model = new OxyPlot.PlotModel();
                model.Title = "Histograma Store";
                model.Axes.Add(new CategoryAxis());
                var series = new OxyPlot.Series.LinearBarSeries();
                model.Series.Add(series);
                Dictionary<int, int> frecuencias = new Dictionary<int, int>();
                for (int i = 1; i < 46; i++)
                {
                    frecuencias.Add(i, datosOrigen.Where(d => d.Store == i).Count());
                }
                series.ItemsSource= frecuencias;
                series.DataFieldX = "Key";
                series.DataFieldY = "Value";
                Histograma1.Model = model;
            }
        }

        private Estadistico CrearEstadistico(List<float> datos, string nombreDeColumna, string tipoDeDato, string tipoDeVariable)
        {
            Estadistico estadistico = new Estadistico();
            estadistico.Conteo = datosOrigen.Count();
            estadistico.Media = datos.Average();
            var ordenados = datos.OrderByDescending(d => d).ToList();
            estadistico.Mediana = ordenados[ordenados.Count() / 2];
            estadistico.Maximo = datos.Max(d => d);
            estadistico.Minimo = datos.Min(d => d);
            estadistico.NombreDeColumna = nombreDeColumna;
            double sum = 0;
            foreach (var item in datos)
            {
                sum = sum + Math.Pow(item - estadistico.Media, 2);
            }
            estadistico.DesviacionEstandard = Math.Sqrt(sum / estadistico.Conteo);
            estadistico.Varianza = sum / estadistico.Conteo;
            estadistico.TipoDeDato = tipoDeDato;
            estadistico.TipoDeVariable = tipoDeVariable;
            estadistico.Caurtil2 = estadistico.Mediana;
            int pos = estadistico.Conteo / 4;
            estadistico.Caurtil1 = ordenados[pos];
            //estadistico.Caurtil2 = ordenados[pos*2];
            estadistico.Caurtil3 = ordenados[pos * 3];
            return estadistico;
        }

        private Estadistico CrearEstadistico(List<int> datos,string nombreDeColumna,string tipoDeDato, string tipoDeVariable)
        {
            Estadistico estadistico = new Estadistico();
            estadistico.Conteo=datosOrigen.Count();
            estadistico.Media = datos.Average();
            var ordenados = datos.OrderByDescending(d => d).ToList();
            estadistico.Mediana = ordenados[ordenados.Count() / 2];
            estadistico.Maximo = datos.Max(d => d);
            estadistico.Minimo = datos.Min(d => d);
            estadistico.NombreDeColumna = nombreDeColumna;
            double sum = 0;
            foreach (var item in datos)
            {
                sum = sum + Math.Pow(item - estadistico.Media, 2);
            }
            estadistico.DesviacionEstandard = Math.Sqrt( sum / estadistico.Conteo);
            estadistico.Varianza = sum/ estadistico.Conteo;
            estadistico.TipoDeDato = tipoDeDato;
            estadistico.TipoDeVariable = tipoDeVariable;
            estadistico.Caurtil2 = estadistico.Mediana;
            int pos = estadistico.Conteo / 4;
            estadistico.Caurtil1 = ordenados[pos];
            //estadistico.Caurtil2 = ordenados[pos*2];
            estadistico.Caurtil3 = ordenados[pos*3];
            return estadistico;
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Guardar resultados";
            saveFileDialog.Filter = "Archivos CSV|*.csv";
            if (saveFileDialog.ShowDialog().Value)
            {
                using (var writer = new StreamWriter(saveFileDialog.FileName))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(estadisticos);
                }
            }
        }
    }
}
